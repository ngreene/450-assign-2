#pragma once

#include "Tracker.h"

class ORB_FLANN_Tracker : public Tracker {

public:

	ORB_FLANN_Tracker();
	~ORB_FLANN_Tracker();

	CameraPose initialise(const std::string& targetFile, const cv::Mat& frame, const CameraCalibration& calibration);
	CameraPose update(const cv::Mat& frame, const CameraCalibration& calibration);
	cv::Size targetSize();

private:

};