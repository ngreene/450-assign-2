#include "ORB_FLANN_Tracker.h"

ORB_FLANN_Tracker::ORB_FLANN_Tracker() : Tracker() {

}

ORB_FLANN_Tracker::~ORB_FLANN_Tracker() {

}

CameraPose ORB_FLANN_Tracker::initialise(const std::string& targetFile, const cv::Mat& frame, const CameraCalibration& calibration) {

	CameraPose pose;
	pose.tracked = false;

	return pose;
}

CameraPose ORB_FLANN_Tracker::update(const cv::Mat& frame, const CameraCalibration& calibration) {
	
	CameraPose pose;
	pose.tracked = false;

	return pose;
}

cv::Size ORB_FLANN_Tracker::targetSize() {
	return cv::Size();
}