#include "KLT_Tracker.h"
#include "SIFT_BF_Tracker.h"

KLT_Tracker::KLT_Tracker() : Tracker(), targetImageSize(), prevPoints(), targetPoints(), prevFrame() {

}

KLT_Tracker::~KLT_Tracker() {

}

CameraPose KLT_Tracker::initialise(const std::string& targetFile, const cv::Mat& frame, const CameraCalibration& calibration) {
	
	// Use a SIFT tracker to determine camera pose
	SIFT_BF_Tracker tempTracker;
	CameraPose pose = tempTracker.initialise(targetFile, frame, calibration);
	targetImageSize = tempTracker.targetSize();

	// Project the target corners into the image
	std::vector<cv::Point3f> targetCorners3d(4);
	std::vector<cv::Point2f> targetCorners2d(4);
	std::vector<cv::Point2f> imageCorners(4);

	float w = targetImageSize.width;
	float h = targetImageSize.height;
	
	targetCorners3d[0] = cv::Point3f(0, 0, 0);
	targetCorners3d[1] = cv::Point3f(w, 0, 0);
	targetCorners3d[2] = cv::Point3f(w, h, 0);
	targetCorners3d[3] = cv::Point3f(0, h, 0);

	targetCorners2d[0] = cv::Point2f(0, 0);
	targetCorners2d[1] = cv::Point2f(w, 0);
	targetCorners2d[2] = cv::Point2f(w, h);
	targetCorners2d[3] = cv::Point2f(0, h);

	// Find 2D image co-ordinates of the target corners 
	cv::projectPoints(targetCorners3d, pose.rvec, pose.tvec, calibration.K, calibration.d, imageCorners);

	// And then a homography from the image to the target image
	cv::Mat H = cv::findHomography(imageCorners, targetCorners2d, cv::noArray(), 0);

	// Find Shi-Tomasi corners
	cv::Mat gray(frame.size(), CV_8UC1);
	cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
	cv::goodFeaturesToTrack(gray, imageCorners, 1000, 0.01, 3);
	

	// See which ones are in the target, and remember them for tracking
	cv::perspectiveTransform(imageCorners, targetCorners2d, H);

	for (int i = 0; i < imageCorners.size(); ++i) {
		bool inTarget =
			targetCorners2d[i].x >= 0 &&
			targetCorners2d[i].x < targetImageSize.width &&
			targetCorners2d[i].y >= 0 &&
			targetCorners2d[i].y < targetImageSize.height;

		if (inTarget) {
			prevPoints.push_back(imageCorners[i]);
			targetPoints.push_back(cv::Point3f(targetCorners2d[i].x, targetCorners2d[i].y, 0));
		}
	}

	prevFrame = gray;
	return pose;

}

CameraPose KLT_Tracker::update(const cv::Mat& frame, const CameraCalibration& calibration) {
	CameraPose pose;
	pose.tracked = false;
	return pose;
}

cv::Size KLT_Tracker::targetSize() {
	return targetImageSize;
}